import json
import boto3
import uuid
import random
from decimal import Decimal

dynamodb = boto3.resource('dynamodb')

# test of ci/cd pipeline

def log_transaction(body, status):
    id_db=uuid.uuid4()
    transaction_table = dynamodb.Table('Transactions')
    transaction_table.put_item(Item={
        'id': str(id_db),
        'merchantName': body.get('merchant_name'),
        'transactionType': body.get('card_type'),
        'CCNum': str(body.get('cc_num'))[-4:],
        'amount': body.get('amount'),
        'time': str(body.get('timestamp')),
        'status': status
    })
    

def lambda_handler(event, context):
    print(event)
    
    
    # returnString = ""
    # if 'body' in event and event['body'] is not None:
    #     body = json.loads(event['body'])
    #     for key in body:
    #         returnString += str(key) + ": " + body[key] + ", "
    #     returnString = returnString[:-2]
    
    if 'body' in event and event['body'] is not None:
        
        body = json.loads(event['body'])
        merchant_name = body.get('merchant_name')
        token = body.get('merchant_token')
        
        merchant_table = dynamodb.Table('Merchants')
        response = merchant_table.scan()
        
        
        if (random.randrange(10) == 0):
            log_transaction(body, 'Error')
            return echo_back("Bank not available.")
        
        # Process the data
        items = response['Items']
        actual_token = None
        for item in items:
            if item.get('MerchantName') == merchant_name:
                actual_token = item.get('Token')
                break
        if not actual_token:
            log_transaction(body, 'Error')
            return echo_back("Merchant not found.")
    
        if token != actual_token:
            log_transaction(body, 'Error')
            return echo_back("Merchant not authorized.")
            
        bank = body.get('bank')
        cc_num = body.get('cc_num')
        card_type = body.get('card_type')
        amount = body.get('amount')
            
        banks_table = dynamodb.Table('Banks')
        credit_table = dynamodb.Table('CCVendors')
        
        if card_type == 'Debit':
            response = banks_table.get_item(Key={'BankName' : bank,'AccountNum' : str(cc_num)})
            if 'Item' not in response:
                log_transaction(body, 'Error')
                return echo_back("Error - Bad Bank or Account Number.")
            account = response['Item']
            if float(account['Balance']) < amount:
                log_transaction(body, 'Declined')
                return echo_back("Declined. Insufficient Funds.")
            else:
                banks_table.update_item(
                    Key={
                        'BankName' : bank,
                        'AccountNum' : str(cc_num)
                    },
                    UpdateExpression='SET Balance = :val1',
                    ExpressionAttributeValues={
                        ':val1': str(float(account['Balance']) - amount)
                    }
                )
                log_transaction(body, 'Approved')
                return echo_back("Approved")
                
        elif card_type == 'Credit':
            response = credit_table.get_item(Key={
                'BankName' : bank,
                'AccountNum' : str(cc_num)
            })
            if 'Item' not in response:
                log_transaction(body, 'Error')
                return echo_back("Error - Bad Bank or Account Number.")
            account = response['Item']
            if (float(account['CreditLimit']) - float(account['CreditUsed'])) < amount:
                log_transaction(body, 'Declined')
                return echo_back("Declined. Insufficient Funds.")
            else:
                credit_table.update_item(
                    Key={
                        'BankName' : bank,
                        'AccountNum' : str(cc_num)
                    },
                    UpdateExpression='SET Balance = :val1',
                    ExpressionAttributeValues={
                        ':val1': str(float(account['CreditUsed']) + amount)
                    }
                )
                log_transaction(body, 'Approved')
                return echo_back("Approved")
    
    
    log_transaction(body, 'Error')
    return echo_back("Invalid Data")
        
    
    # try:
    #     if event:
    #         for key in event:
    #             returnString += str(key) + ": " + event[key] + ", "
    #         returnString = returnString[:]
    # except KeyError:
    #     print('KeyError')
    
def echo_back(returnString):
    return {
        'statusCode': 200,
        'headers': {
            "Content-Type": "text/html"
        },
        'body': returnString
    }